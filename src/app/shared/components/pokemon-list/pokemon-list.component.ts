import {Component, Input} from '@angular/core';
import {Pokemon} from '../../models/pokemon.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['pokemon-list.component.css']
})

export class PokemonListComponent {

  constructor(private readonly router: Router) {}
  // tslint:disable-next-line:no-input-rename
  @Input('pokemonList') pokemonList: Pokemon[];

  onPokemonClick(id: string): Promise<boolean> {
    return this.router.navigate([`pokemon/${id}`]);
  }
}
