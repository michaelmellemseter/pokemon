export enum AppRoutes {
  Login = 'login',
  PokemonCatalogue = 'catalogue',
  PokemonDetail = 'pokemon/:id',
  Trainer = 'trainer',
  NotFound = '**'
}
