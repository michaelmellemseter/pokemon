import {NgModule} from '@angular/core';
import {PokemonListComponent} from './components/pokemon-list/pokemon-list.component';
import {CommonModule} from '@angular/common';
import {NavbarComponent} from './components/navbar/navbar.component';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    PokemonListComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    CommonModule,
    PokemonListComponent,
    NavbarComponent
  ]
})
export class SharedModule {}
