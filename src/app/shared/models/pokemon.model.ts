export interface Pokemon {
  id?: string;
  name?: string;
  url?: string;
  image?: string;
  height?: number;
  weight?: number;
  base_experience?: number;
  abilities?: Ability[];
  types?: Type[];
  stats?: Stat[];
  moves?: Move[];
}

export interface Type {
  slot: number;
  type: TypeType;
}

export interface TypeType {
  name: string;
  url: string;
}

export interface Ability {
  slot: number;
  is_hidden: boolean;
  ability: AbilityAbility;
}

export interface AbilityAbility {
  name: string;
  url: string;
}

export interface Stat {
  base_stat: number;
  effort: number;
  stat: StatStat;
}

export interface StatStat {
  name: string;
  url: string;
}

export interface Move {
  move: MoveMove;
  version_group_details: VersionGroupDetails;
}

export interface MoveMove {
  name: string;
  url: string;
}

export interface VersionGroupDetails {
  level_learned_at: number;
  move_learn_method: MoveLearnMethod;
  version_group: VersionGroup;
}

export interface MoveLearnMethod {
  name: string;
  url: string;
}

export interface VersionGroup {
  name: string;
  url: string;
}
