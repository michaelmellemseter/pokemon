import {Injectable} from '@angular/core';
import {StorageKeys} from '../enums/storage-keys';

@Injectable({
  providedIn: 'root'
})

export class SessionService {
  active(): boolean {
    const trainer = localStorage.getItem(StorageKeys.Trainer);
    return Boolean(trainer);
  }
}
