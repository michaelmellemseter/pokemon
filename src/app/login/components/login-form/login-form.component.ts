import {Component, EventEmitter, Output} from '@angular/core';
import {StorageKeys} from '../../../shared/enums/storage-keys';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html'
})

export class LoginFormComponent {
  @Output() success: EventEmitter<void> = new EventEmitter<void>();
  public trainername: string = '';

  public onLoginClick(): void {
    window.localStorage.setItem(StorageKeys.Trainer, this.trainername);
    window.localStorage.setItem(StorageKeys.Pokemon, JSON.stringify([]));
    this.success.emit();
  }
}
