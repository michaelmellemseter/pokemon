import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AppRoutes} from '../../../shared/enums/app-routes.enum';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html'
})

export class LoginPage {
  constructor(private readonly router: Router) {
  }

  handleLogin(): Promise<boolean> {
    return this.router.navigate( [AppRoutes.PokemonCatalogue]);
  }
}
