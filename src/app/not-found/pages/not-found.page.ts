import {Component} from '@angular/core';
import {AppRoutes} from '../../shared/enums/app-routes.enum';

@Component({
  selector: 'app-not-found-page',
  templateUrl: 'not-found.page.html'
})

export class NotFoundPage {
  catalogueRoute: string = AppRoutes.PokemonCatalogue;
}
