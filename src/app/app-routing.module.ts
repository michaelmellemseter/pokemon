import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppRoutes} from './shared/enums/app-routes.enum';
import {SessionGuard} from './guards/session.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: `/${AppRoutes.Login}`,
  },
  {
    path: AppRoutes.Login,
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
    resolve: [SessionGuard]
  },
  {
    path: AppRoutes.PokemonCatalogue,
    loadChildren: () => import('./catalogue/catalogue.module').then(m => m.CatalogueModule),
    canActivate: [SessionGuard]
  },
  {
    path: AppRoutes.PokemonDetail,
    loadChildren: () => import('./pokemon-details/pokemon-details.module').then((m => m.PokemonDetailsModule)),
    canActivate: [SessionGuard]
  },
  {
    path: AppRoutes.Trainer,
    loadChildren: () => import('./trainer/trainer.module').then(m => m.TrainerModule),
    canActivate: [SessionGuard]
  },
  {
    path: AppRoutes.NotFound,
    loadChildren: () => import('./not-found/not-found.module').then(m => m.NotFoundModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
