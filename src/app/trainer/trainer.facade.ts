import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Pokemon} from '../shared/models/pokemon.model';
import {TrainerState} from './state/trainer.state';
import {StorageKeys} from '../shared/enums/storage-keys';

@Injectable({
  providedIn: 'root'
})

export class TrainerFacade {

  constructor(private trainerState: TrainerState ) {}

  pokemon$(): Observable<Pokemon[]> {
    return this.trainerState.getPokemon$();
  }

  getPokemon(): void {
    try{
      const pokemon: Pokemon[] = JSON.parse(localStorage.getItem(StorageKeys.Pokemon));
      console.log(pokemon);
      this.trainerState.setPokemon$(pokemon);
    }
    catch (error) {
      this.trainerState.setError$(error.message);
    }
  }
}
