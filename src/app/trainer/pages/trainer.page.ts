import {Component, OnInit} from '@angular/core';
import {TrainerFacade} from '../trainer.facade';
import {Observable} from 'rxjs';
import {Pokemon} from '../../shared/models/pokemon.model';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer.page.html'
})

export class TrainerPage implements OnInit {

  constructor(private readonly trainerFacade: TrainerFacade) {}

  get pokemon$(): Observable<Pokemon[]> {
    return this.trainerFacade.pokemon$();
  }

  ngOnInit(): void{
    this.trainerFacade.getPokemon();
  }
}
