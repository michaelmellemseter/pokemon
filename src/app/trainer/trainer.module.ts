import {NgModule} from '@angular/core';
import {TrainerPage} from './pages/trainer.page';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {TrainerRoutingModule} from './trainer-routing.module';

@NgModule({
  declarations: [
    TrainerPage
  ],
  imports: [
    TrainerRoutingModule,
    CommonModule,
    SharedModule
  ]
})

export class TrainerModule {}
