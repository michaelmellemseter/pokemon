import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {PokemonResponse} from '../../shared/models/pokemon-response.model';

const { pokemonApiBaseUrl } = environment;

@Injectable({
  providedIn: 'root'
})

export class CatalogueApi {
  constructor(private readonly http: HttpClient) {}

  public fetchPokemon$(): Observable<PokemonResponse> {
    return this.http.get<PokemonResponse>(`${pokemonApiBaseUrl}`);
  }
}
