import {Component, OnInit} from '@angular/core';
import {CatalogueFacade} from '../catalogue.facade';
import {Observable} from 'rxjs';
import {Pokemon} from '../../shared/models/pokemon.model';

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue.page.html',
})

export class CataloguePage implements OnInit {

  constructor(private readonly catalogueFacade: CatalogueFacade) {}

  get pokemon$(): Observable<Pokemon[]> {
    return this.catalogueFacade.pokemon$();
  }

  ngOnInit(): void {
    this.catalogueFacade.fetchPokemon();
  }
}
