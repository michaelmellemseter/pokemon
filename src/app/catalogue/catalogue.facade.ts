import {Injectable} from '@angular/core';
import {CatalogueApi} from './api/catalogue.api';
import {Pokemon} from '../shared/models/pokemon.model';
import {HttpErrorResponse} from '@angular/common/http';
import {CatalogueState} from './state/catalogue.state';
import {PokemonResponse} from '../shared/models/pokemon-response.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class CatalogueFacade {
  constructor(private readonly catalogueAPI: CatalogueApi, private catalogueState: CatalogueState) {}

  pokemon$(): Observable<Pokemon[]> {
    return this.catalogueState.getPokemon$();
  }

  fetchPokemon(): void {
    this.catalogueAPI.fetchPokemon$()
      .subscribe((res: PokemonResponse) => {
        const returnPokemon: Pokemon[] = [];
        for (const pokemon of res.results) {
          const newPokemon: Pokemon = this.getIdAndImage(pokemon.url);
          newPokemon.name = pokemon.name;
          returnPokemon.push(newPokemon);
        }
        console.log(returnPokemon);
        this.catalogueState.setPokemon(returnPokemon);
      }, (err: HttpErrorResponse) => {
        this.catalogueState.setError(err.message);
      });
  }

  private getIdAndImage(url: string): Pokemon {
    const id = url.split( '/' ).filter( Boolean ).pop();
    return {id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }

}
