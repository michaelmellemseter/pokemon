import {NgModule} from '@angular/core';
import {CataloguePage} from './pages/catalogue.page';
import {CatalogueRoutingModule} from './catalogue-routing.module';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    CataloguePage
  ],
  imports: [
    CatalogueRoutingModule,
    CommonModule,
    SharedModule
  ]
})

export class CatalogueModule {}
