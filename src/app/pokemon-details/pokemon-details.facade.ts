import {Injectable} from '@angular/core';
import {PokemonDetailsApi} from './api/pokemon-details.api';
import {PokemonDetailsState} from './state/pokemon-details.state';
import {Observable} from 'rxjs';
import {Ability, Pokemon} from '../shared/models/pokemon.model';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class PokemonDetailsFacade {
  constructor(private readonly pokemonDetailApi: PokemonDetailsApi, private pokemonDetailState: PokemonDetailsState) {}

  pokemon$(): Observable<Pokemon> {
    return this.pokemonDetailState.getPokemon$();
  }

  fetchPokemon(id: number): void {
    this.pokemonDetailApi.fetchPokemonById$(id)
      .subscribe((res: Pokemon) => {
        console.log(res);
        // const ability: Ability = res.abilities[0];
        const pokemon: Pokemon = {id: res.id, name: res.name, image: res.image, height: res.height, weight: res.weight, base_experience: res.base_experience, abilities: res.abilities, types: res.types, stats: res.stats, moves:res.moves};
        this.pokemonDetailState.setPokemon(pokemon);
      }, (err: HttpErrorResponse) => {
        this.pokemonDetailState.setError(err.message);
      });
  }
}
