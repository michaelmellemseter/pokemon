import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Pokemon} from '../../shared/models/pokemon.model';

@Injectable({
  providedIn: 'root'
})

export class PokemonDetailsState {
  private readonly pokemon$: BehaviorSubject<Pokemon> = new BehaviorSubject<Pokemon>(null);
  private readonly error$: BehaviorSubject<string> = new BehaviorSubject<string>('');

  getPokemon$(): Observable<Pokemon> {
    return this.pokemon$.asObservable();
  }

  setPokemon(pokemon: Pokemon): void {
    this.pokemon$.next(pokemon);
  }

  getError(): Observable<string> {
    return this.error$.asObservable();
  }

  setError(error: string): void {
    this.error$.next(error);
  }
}
