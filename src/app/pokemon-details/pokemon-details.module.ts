import {NgModule} from '@angular/core';
import {PokemonDetailsPage} from './pages/pokemon-details.page';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PokemonDetailsRoutingModule} from './pokemon-details-routing.module';
import {PokemonBaseComponent} from './components/pokemon-base/pokemon-base.component';
import {PokemonMovesComponent} from './components/pokemon-moves/pokemon-moves.component';
import {PokemonProfileComponent} from './components/pokemon-profile/pokemon-profile.component';

@NgModule({
  declarations: [
    PokemonDetailsPage,
    PokemonBaseComponent,
    PokemonMovesComponent,
    PokemonProfileComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    PokemonDetailsRoutingModule
  ]
})

export class PokemonDetailsModule {}
