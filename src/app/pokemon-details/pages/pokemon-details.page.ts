import {Component, OnInit} from '@angular/core';
import {PokemonDetailsFacade} from '../pokemon-details.facade';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {Pokemon} from '../../shared/models/pokemon.model';
import {AppRoutes} from '../../shared/enums/app-routes.enum';
import {StorageKeys} from '../../shared/enums/storage-keys';

@Component({
  selector: 'app-pokemon-detail-page',
  templateUrl: './pokemon-details.page.html',
  styleUrls: ['pokemon-details.page.css']
})

export class PokemonDetailsPage implements OnInit {

  private readonly pokemonId: number = 0;

  constructor(private readonly route: ActivatedRoute,
              private readonly pokemonDetailFacade: PokemonDetailsFacade,
              private readonly router: Router) {
    this.pokemonId = Number(this.route.snapshot.paramMap.get('id'));
  }

  onAddToCollectionClick(pokemon: Pokemon): Promise<boolean> {
    const pokemons = JSON.parse(localStorage.getItem(StorageKeys.Pokemon));
    pokemons.push(pokemon);
    localStorage.setItem(StorageKeys.Pokemon, JSON.stringify(pokemons));
    return this.router.navigate( [AppRoutes.Trainer]);
  }

  ngOnInit(): void {
    this.pokemonDetailFacade.fetchPokemon(this.pokemonId);
  }

  get pokemon$(): Observable<Pokemon> {
    return this.pokemonDetailFacade.pokemon$();
  }
}
