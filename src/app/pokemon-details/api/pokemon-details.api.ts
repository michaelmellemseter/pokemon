import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Pokemon} from '../../shared/models/pokemon.model';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';

const { pokemonApiBaseUrl } = environment;

@Injectable({
  providedIn: 'root'
})

export class PokemonDetailsApi {
  constructor(private readonly http: HttpClient) {}

  public fetchPokemonById$(id: number): Observable<Pokemon> {
    return this.http.get<Pokemon>(`${pokemonApiBaseUrl}/${id}`)
      .pipe(
        map((pokemon: Pokemon) => ({
          ...pokemon,
          image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ pokemon.id }.png`,
          abilities: pokemon.abilities
        }))
      );
  }
}
