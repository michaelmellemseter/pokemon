import {Component, Input} from '@angular/core';
import {Pokemon} from '../../../shared/models/pokemon.model';

@Component({
  selector: 'app-pokemon-base',
  templateUrl: './pokemon-base.component.html'
})

export class PokemonBaseComponent {
  @Input('pokemon') pokemon: Pokemon;
}
