import {Component, Input} from '@angular/core';
import {Pokemon} from '../../../shared/models/pokemon.model';

@Component({
  selector: 'app-pokemon-moves',
  templateUrl: './pokemon-moves.component.html'
})
export class PokemonMovesComponent {
  @Input('pokemon') pokemon: Pokemon;
}
